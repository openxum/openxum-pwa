import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Admin from '@/components/Admin'
import Games from '@/components/Games'
import MyGames from '@/components/MyGames'
import Login from '@/components/Login'
import SignUp from '@/components/SignUp'
import Profile from '@/components/Profile'
import Faq from '@/components/Faq'

import Create from '@/components/games/Create'
import Play from '@/components/games/Play'
import RulesAbande from '@/components/games/abande/Rules'
import RulesDakapo from '@/components/games/dakapo/Rules'
import RulesDvonn from '@/components/games/dvonn/Rules'
import RulesGipf from '@/components/games/gipf/Rules'
import RulesHnefatafl from '@/components/games/hnefatafl/Rules'
import RulesInvers from '@/components/games/invers/Rules'
import RulesKamisado from '@/components/games/kamisado/Rules'
import RulesLyngk from '@/components/games/lyngk/Rules'
import RulesManalath from '@/components/games/manalath/Rules'
import RulesMixtour from '@/components/games/mixtour/Rules'
import RulesNeutreeko from '@/components/games/neutreeko/Rules'
import RulesOrdo from '@/components/games/ordo/Rules'
import RulesPaletto from '@/components/games/paletto/Rules'
import RulesPentago from '@/components/games/pentago/Rules'
import RulesTintas from '@/components/games/tintas/Rules'
import RulesTzaar from '@/components/games/tzaar/Rules'
import RulesYinsh from '@/components/games/yinsh/Rules'
import RulesZertz from '@/components/games/zertz/Rules'

Vue.use(Router)

const router = new Router({
  routes: [
    {path: '/', redirect: {name: 'Home'}},
    {
      path: '/home',
      name: 'Home',
      component: Home
    },
    {
      path: '/admin',
      name: 'Admin',
      component: Admin,
      meta: {requiresAuth: true, adminAuth: true}
    },
    {
      path: '/games',
      name: 'Games',
      component: Games
    },
    {
      path: '/mygames',
      name: 'MyGames',
      component: MyGames,
      meta: {requiresAuth: true, playerAuth: true}
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/signup',
      name: 'SignUp',
      component: SignUp
    },
    {
      path: '/faq',
      name: 'Faq',
      component: Faq
    },
    {
      path: '/profile',
      name: 'Profile',
      component: Profile,
      meta: {requiresAuth: true, playerAuth: true}
    },
    {
      path: '/create/:gameType',
      name: 'Create',
      component: Create,
      meta: {requiresAuth: true, playerAuth: true}
    },
    {
      path: '/games/play/:gameType/:type/:color/:mode/:idGame/:idOpponent',
      name: 'Play',
      component: Play,
      meta: {requiresAuth: true, playerAuth: true}
    },
    {
      path: '/games/rules/abande',
      name: 'RulesAbande',
      component: RulesAbande
    },
    {
      path: '/games/rules/dakapo',
      name: 'RulesDakapo',
      component: RulesDakapo
    },
    {
      path: '/games/rules/dvonn',
      name: 'RulesDvonn',
      component: RulesDvonn
    },
    {
      path: '/games/rules/gipf',
      name: 'RulesGipf',
      component: RulesGipf
    },
    {
      path: '/games/rules/hnefatafl',
      name: 'RulesHnefatafl',
      component: RulesHnefatafl
    },
    {
      path: '/games/rules/invers',
      name: 'RulesInvers',
      component: RulesInvers
    },
    {
      path: '/games/rules/kamisado',
      name: 'RulesKamisado',
      component: RulesKamisado
    },
    {
      path: '/games/rules/lyngk',
      name: 'RulesLyngk',
      component: RulesLyngk
    },
    {
      path: '/games/rules/manalath',
      name: 'RulesManalath',
      component: RulesManalath
    },
    {
      path: '/games/rules/mixtour',
      name: 'RulesMixtour',
      component: RulesMixtour
    },
    {
      path: '/games/rules/neutreeko',
      name: 'RulesNeutreeko',
      component: RulesNeutreeko
    },
    {
      path: '/games/rules/ordo',
      name: 'RulesOrdo',
      component: RulesOrdo
    },
    {
      path: '/games/rules/paletto',
      name: 'RulesPaletto',
      component: RulesPaletto
    },
    {
      path: '/games/rules/pentago',
      name: 'RulesPentago',
      component: RulesPentago
    },
    {
      path: '/games/rules/tintas',
      name: 'RulesTintas',
      component: RulesTintas
    },
    {
      path: '/games/rules/tzaar',
      name: 'RulesTzaar',
      component: RulesTzaar
    },
    {
      path: '/games/rules/yinsh',
      name: 'RulesYinsh',
      component: RulesYinsh
    },
    {
      path: '/games/rules/zertz',
      name: 'RulesZertz',
      component: RulesZertz
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.meta.requiresAuth) {
    const authUser = JSON.parse(window.localStorage.getItem('openXumUser'))

    if (!authUser || !authUser.token) {
      next('/login')
    } else if (to.meta.adminAuth) {
      const authUser = JSON.parse(window.localStorage.getItem('openXumUser'))

      if (authUser.data.role === 'admin') {
        next()
      } else {
        next('/')
      }
    } else if (to.meta.playerAuth) {
      const authUser = JSON.parse(window.localStorage.getItem('openXumUser'))

      if (authUser.data.role === 'player') {
        next()
      } else {
        next('/admin')
      }
    }
  } else {
    next()
  }
})

export default router
