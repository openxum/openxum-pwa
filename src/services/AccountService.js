import { APIENDPOINT, getHeader } from '../app.config'
import axios from 'axios'

export default {
  login (value, cb) {
    return new Promise(function (resolve, reject) {
      axios.post(APIENDPOINT + '/login', value, {
        'Accept': 'application/json'
      })
        .then(function (res) {
          resolve(res)
        })
        .catch(function (err) {
          reject(err)
        })
    })
  },
  create (value, cb) {
    return new Promise(function (resolve, reject) {
      axios.post(APIENDPOINT + '/create', value, {
        'Accept': 'application/json'
      })
        .then(function (res) {
          resolve(res)
        })
        .catch(function (err) {
          reject(err)
        })
    })
  },
  delete (value, cb) {
    return new Promise(function (resolve, reject) {
      const config = {'headers': getHeader()}

      axios.post(APIENDPOINT + '/api/user/delete/' + value.id, value, config)
        .then(function (res) {
          resolve(res)
        })
        .catch(function (err) {
          reject(err)
        })
    })
  },
  update (value, cb) {
    return new Promise(function (resolve, reject) {
      const config = {'headers': getHeader()}

      axios.put(APIENDPOINT + '/api/user/' + value.id + '/update', value, config)
        .then(function (res) {
          resolve(res)
        })
        .catch(function (err) {
          reject(err)
        })
    })
  }
}
